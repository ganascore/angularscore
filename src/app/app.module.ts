import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ListapartidosComponent } from './components/listapartidos/listapartidos.component';

//routes
import {APP_ROUTING} from "./app.routes";
import { AuthComponent } from './components/auth/auth.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { LeftsidebarComponent } from './components/apuesta/leftsidebar/leftsidebar.component';
import { ListeventComponent } from './components/apuesta/listevent/listevent.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ListapartidosComponent,
    AuthComponent,
    FooterComponent,
    LeftsidebarComponent,
    ListeventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
