import {RouterModule, Routes} from "@angular/router";
import {ListapartidosComponent} from "./components/listapartidos/listapartidos.component";
import {AuthComponent} from "./components/auth/auth.component";


const APP_ROUTES:Routes = [
  {path: 'Lista', component: ListapartidosComponent},
  {path: 'Login', component: AuthComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'Login'}
];


export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
